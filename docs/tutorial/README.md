

# 視窗管理

View 用來指定一個可視區域(viewable area)，相當於視窗系統(window system)當中的視窗(window)．

Workspace 本身也是一個可視區域，可以在這個可視區域內建立與管理多個可視區域，相當於視窗系統(window system)當中的視窗管理員(window manager)或桌面環境(desktop environment)．

![Workspace+View](./Tutorial/Workspace+View.png)

# General

## Panel

### 屬性

#### badge

可以是 數字、文字或是 Icon(<Icon icon='edit' small={true"}/>)．

### 範例

```jsx
<Panel title={props.title} collapsable={true} width={props.width} badge={12}>
	<Tree ref={props.tree} data={props.data} idAccessor="path" parentAccessor="folder" selection={props.selection} onSelect={props.onSelect}>
    <Column title="Name" accessor="name" />
    <Column title="Kind" accessor="kind" width={200} />
    <Column title="Modified" accessor="modifiedOn" width={200}/>
</Tree>
</Panel>
```

```jsx
<Panel title={props.title} collapsable={true} width={props.width} badge={<Icon icon='edit' small={true"}/>}>
	<Tree ref={props.tree} data={props.data} idAccessor="path" parentAccessor="folder" selection={props.selection} onSelect={props.onSelect}>
    <Column title="Name" accessor="name" />
    <Column title="Kind" accessor="kind" width={200} />
    <Column title="Modified" accessor="modifiedOn" width={200}/>
</Tree>
</Panel>
```



## Collapse

需要指定一個操作 Collapse 的元件與顯示的內容．

### 屬性

#### title：String

#### content: Element

Collapse 元件要顯示的內容．

#### defaultCollapsed: Boolean

指定預設是否要顯示內容．

#### isCollapsed: Boolean

指定使用 controlled mode 操作 Collapse．

#### children

在沒有指定 title 時，Collapse 將使用 children 的第一個元件作為操作元件；沒有指定 content 時，會用第二個元件作為顯示內容．

### 範例

最簡單的做法，由 Collapse 元件提供內建的操作元件顯示 Table．

```jsx
<Collapse title="All Products">
  <Table data={this.state.data} selectioin={this.state.selection} onSelect={this.onSelect} >
    <Column title="Name" accessor="name" editable={true} flex={2} sortable={true} />
    <Column title="Category" accessor="category" code="category" flex={2} sortable={true} />
    <Column title="Description" accessor="description" wrapText={false} flex={4}/>
    <Column title="Price" accessor="price" minWidth={100} sortable={true} />
    <Column title="Dimension" renderer={cellRenderer} flex={2} />
  </Table>
</Collapse>
```

使用指定的 Button 來操作 Collapse 顯示 Table．

```jsx
<Collapse>
  <Button text="All Products" />
  <Table data={this.state.data} selectioin={this.state.selection} onSelect={this.onSelect} >
    <Column title="Name" accessor="name" editable={true} flex={2} sortable={true} />
    <Column title="Category" accessor="category" code="category" flex={2} sortable={true} />
    <Column title="Description" accessor="description" wrapText={false} flex={4}/>
    <Column title="Price" accessor="price" minWidth={100} sortable={true} />
    <Column title="Dimension" renderer={cellRenderer} flex={2} />
  </Table>
</Collapse>
```



## Label

### 屬性

#### align : Alignment | string

指定標籤的對齊方式．

## Table

表格元件．

### 屬性

#### mode : SelectionMode 

指定 Table 的選擇模式．預設值是 SelectionMode.ROWS．

#### multiple : boolean

指定 Table 是否可以多選．預設值是 false．

#### selection : Table.Selection

指定選擇的區域．

#### rowHeaderStyle : Table.RowHeaderStyle | boolean

指定資料列標題的型式．預設值是：RowHeaderStyle.NUMBERING．

可以選擇的值：

- RowHeaderStyle.NONE 或 false：不顯示資料列的標題．
- RowHeaderStyle.CHECKBOX：顯示用來選取資料列的 checkbox．table 必須是在可以選擇資料列的模式．
- RowHeaderStyle.NUMBERING：顯示資料列的編號．

#### columnHeader : boolean

指定是否顯示資料欄的標題．預設值是：true．

#### gridline: Table.Gridline

指定 Table 的格線型式．預設值是：Gridline.BOTH．

可以選擇的值：

- Gridline.BOTH
- Gridline.HORIZONTAL

#### wrapText: boolean

指定該欄的文字是否折行(wrap). 預設是 True.

### 事件

#### onSelect(event)

event.selections 代表選擇的區域．

### 相關類別

#### Table.Column

#### 屬性

**dataType : DataType**

指定該欄的資料型別．

**sortable : boolean**

指定該欄是否可以排序．預設值是 false．

**pattern: numeral.Pattern**

指定該欄資料的顯示所使用的格式.

如果是數字, 可以指定的格式有：

- 'decimal': 以十進位數字的方式顯示. 這是預設值.
- 'currency': 以貨幣的格式顯示
- 'percentage' : 以百分比的方式顯示.

**grouping: boolean**

指定該欄如果是數字時，是否使用千位分隔符號．預設是 true.

**scale: number**

指定該欄如果是數字時，小數點以後的位數．

**currency: numeral.Currency**

指定該欄使用的貨幣符號．預設值來自 app.json 的設定．

**accounting: boolean**

指定該欄是否使用會計的負數表示法．預設值是 false．

**compact: boolean**

指定該欄是否使用較簡潔的方式顯示數值．例如：將 3,000 的數值以 3K 顯示，USD 3,000 會以 $3K 表示．預設值是 false．

### Table.Selections

#### 方法

**int[] rows()**

回傳陣列，代表被選擇的列(row)．

**int row()**

回傳被選擇的第一列．

**void addRow(row: int)**

新增一個被選擇的列．

**void addColumn(column: int)**

新增一個選擇的列．

**void addRegion(rows: int[], cols: int[])**

新增一個選擇的區域．

### 範例

```jsx
<Table colspan={3}>
  <Table.Column title="Username" accessor="username" />
  <Table.Column title="Text" accessor="text" />
	<Table.Column title="CreatedOn" accessor="createdOn" dataType={DataType.DATETIME} />
 	<Table.Column title="Priority" accessor="priority" dataType={DataType.FLOAT} />
	<Table.Column title="Read" accessor="read" dataType={DataType.BOOLEAN} />
</Table>
```

# Input

## MultiDatebox

用來處理日期陣列的欄位．

### 屬性

**name : string**

與 Form 搭配時，指定用來綁定資料欄位名稱．

**value : date[]**

不與 Form 搭配，單獨使用時，用來指定要顯示的值．

### 事件

**onChange(event)**

在欄位內容改變的時候觸發．

event.value 代表變更後的新值．如果 `multiple` = true，event.value 是一個陣列，否則是一個 URL 或物件．

event.name 代表欄位的名稱．

## Text

以文字的方式顯示資料．日期的值會使用系統預設的日期格式顯示指定的數值．

### 屬性

#### name : string

與 Form 搭配時，指定用來綁定資料欄位名稱．

#### value : string | number | date

不與 Form 搭配，單獨使用時，用來指定要顯示的值．

#### code : string

指定用來做代碼轉換的代碼種類．

#### Intent : Intent

指定顯示文字的顏色．

## FileInput

用來處理檔案存取的欄位．

### 屬性

**multiple : boolean** 

指定該欄位是否允許存取多的檔案．預設值是 false．

**target : Element** 

另一種指定該欄位的 children 屬性的方法，優先於 children．

### 事件

**onChange(event)**

在欄位內容改變的時候觸發．

event.value 代表變更後的新值．如果 `multiple` = true，event.value 是一個陣列，否則是一個 URL 或物件．

event.name 代表欄位的名稱．

### 範例

單一檔案的範例：

```jsx
<FileInput name="datasheet">
  <Button icon="download" minimal={true} />
</FileInput>
```

多個檔案的範例：

```jsx
<FileInput name="pictures" accept="image/*" multiple={true}>
  <Button icon="download" minimal={true} />
</FileInput>
```

## Worksheet

可以編輯資料的表格元件．

### 屬性

####  resizable : boolean

指定Worksheet是否可以增加或刪除列．預設值是 true．

#### readOnly : boolean

唯讀．

#### mode : SelectionMode 

指定 Worksheet 的選擇模式．預設值是 SelectionMode.ROWS．

#### multiple : boolean

指定 Worksheet 是否可以多選．預設值是 false．

#### selections : Worksheet.Selections

指定選擇的區域．

#### maxHeight : number

指定最大高度．內容超過高度時，可以使用捲動軸操作．

### 事件

#### onSelect(event)

event.selections 代表選擇的區域．

### 相關類別

#### Worksheet.Column

#### 屬性

**editable : boolean** 

指定該欄是否可以編輯，但是只有同時具備資料型別的欄位才可以編輯．預設值是 true．

**dataType : DataType**

指定該欄的資料型別．

**sortable : boolean**

指定該欄是否可以排序．預設值是 false．

### Worksheet.Selections

#### 方法

**int[] rows()**

回傳陣列，代表被選擇的列(row)．

**int row()**

 回傳被選擇的第一列．

**void addRow(row: int)**

新增一個被選擇的列．

**void addColumn(column: int)**

新增一個選擇的列．

**void addRegion(rows: int[], cols: int[])**

新增一個選擇的區域．

### 範例

```jsx
<Worksheet name="comments" colspan={3}>
  <Worksheet.Column title="Username" accessor="username" />
  <Worksheet.Column title="Text" accessor="text" />
	<Worksheet.Column title="CreatedOn" accessor="createdOn" dataType={DataType.DATETIME} />
 	<Worksheet.Column title="Priority" accessor="priority" dataType={DataType.FLOAT} />
	<Worksheet.Column title="Read" accessor="read" dataType={DataType.BOOLEAN} />
</Worksheet>
```



# Layout

## Grid

使用表格方式排列畫面的元件．

### 屬性

#### columns

指定欄位數量

### 方法

無

### 範例

```jsx
<Grid columns={5}>
</Grid>
```

# Util

## downloader

提供檔案下載的功能．

### 方法

**void save(any: Link|String|File|Response|Blob, filename:string)**

下載指定的檔案並儲存到本機的磁碟．

### 範例

下面範例先透過 `request` 讀取 `resource` (product) ，然後使用 `resource.get()` 取得檔案的連結( `link`，attachment)，然後再呼叫 `downloader.save()` 下載檔案，並且以伺服端檔案資源 (`resource`) 的檔名儲存到本機的磁碟．

```javascript
const response = await request.get(link);
const product = Resource.parse(await response.text());
const link = resource.get('attachment');
downloader.save(link);
```



以下範例是使用 `request` 讀取含有檔案內容的 `response`，然後再呼叫 `downloader.save()`，使用指定的檔名將 `response` 內的檔案內容儲存到本機的磁碟．

```javascript
// 透過 request.get() 從伺服器下載檔案.
const predicate = Predicate.startsWith('name', criteria.name)
	.and(Predicate.in('category', criteria.category))
	.and(Predicate.has('keywords', criteria.keywords))
	.and(Predicate.ge('price', criteria.price))
let response = await request.get(link, {predicate});
// 將下載的檔案內容(response) 儲存到 product.xlsx 的檔案.
downloader.save(response, 'product.xlsx');
```



## Time

### 範例

計算指定的開始與結束時間期間的天數與小時數. 

```javascript
import time from '../util/time';

let startTime = time.of(2010, 1, 2, 8);
let endTime = time.of(2010, 1, 3, 16);
let duration = time.between(startTime, endTime);
let days = duration.days();
let hours = duration.hours();

console.log(`Duration: ${days} Days ${hours} Hours`);
// Duration: 1 Days 8 Hours
```

計算開始時間加上指定的天數與小時數後. 
```javascript
import time from '../util/time';

let startTime = time.of(2010, 1, 2, 8);
let days = 1;
let hours = 8;

let endTime = time.plusDays(startTime, days);
endTime = time.plusHours(endTime, hours);

console.log(`Endtime: ${endTime}`);
// Endtime: Sun Jan 03 2010 16:00:00 GMT+0800 (Taipei Standard Time)
```

# Chart

## Line Chart

```jsx
class MonthlyRevenue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    async componentDidMount() {
        let data = await this.fetchData();
        this.setState({data});
    }

    render() {
        let icon = 'timeline-line-chart';
        let title = 'Monthly Revenue';
        let footnote = new Date().toUTCString();
        let content = this.renderChart();
        return (
            <Dashlet icon={icon} title={title} footnote={footnote} onClick>
                {content}
            </Dashlet>
        );
    }

    onClick(event) {
        this.start();
    }

    renderChart() {
        let data = this.state.data;
        return (
            <LineChart height={250} data={data}>
                <LineChart.XAxis accessor="date" />
                <LineChart.YAxis />
                <LineChart.Gridline />
                <Tooltip/>
                <LineChart.LineSeries key={0} accessor="盒花酒禮" />
                <LineChart.LineSeries key={1} accessor="浪漫粉聖誕樹" />
            </LineChart>
        );
    }

    async fetchData() {
        let products = {
            '盒花酒禮': 4500,
            '浪漫粉聖誕樹': 4000
        };
        let data = [];
        let now = time.now();
        let year = now.getFullYear();
        let r = random.normal(200, 50);

        for(let month=1; month<=12; month++) {
            if(month===4||month===9) continue;
            let date = time.of(year, month, time.daysInMonth(year, month));
            let datum = {
                date: date
            };

            _.each(products, (price, product) => {
                datum[product] = price * Math.floor(r());
            });

            data.push(datum);
        }
        console.log(JSON.stringify(data));
        return data;
    }
}
```



## Bar Chart

```jsx
class YOYRevenue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    async componentDidMount() {
        let data = await this.fetchData();
        this.setState({data});
    }

    render() {
        let icon = 'timeline-bar-chart';
        let title = 'YOY Revenue';
        let footnote = new Date().toUTCString();
        let content = this.renderChart();
        return (
            <Dashlet icon={icon} title={title} footnote={footnote}>
                {content}
            </Dashlet>
        );
    }

    renderChart() {
        let data = this.state.data;
        return (
            <BarChart height={250} data={data}>
                <BarChart.XAxis accessor="product" />
                <BarChart.YAxis />
                <BarChart.Gridline />
                <Tooltip/>
                <BarChart.BarSeries key={0} accessor="Y2019" />
                <BarChart.BarSeries key={1} accessor="Y2018" />
                <BarChart.BarSeries key={2} accessor="Y2017" />
            </BarChart>
        );
    }
    async fetchData() {
        let products = {
            '浪漫華爾滋': 3200,
            '玫瑰訴情': 2500,
            '加州陽光': 2520,
            '盒花酒禮': 4500,
            '浪漫粉聖誕樹': 4000
        };
        let r2019 = random.normal(2000, 500);
        let r2018 = random.normal(2000, 500);
        let r2017= random.normal(2000, 500);

        let data = _.map(products, (price, name) => {
            let datum = {
                product: name,
                'Y2019': price * Math.floor(r2019()),
                'Y2018': price * Math.floor(r2018()),
                'Y2017': price * Math.floor(r2017()),
            };
            return datum;
        });
        return data;
    }
}
```



## Pie Chart

```jsx
class YTDRevenue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    async componentDidMount() {
        let data = await this.fetchData();
        this.setState({data});
    }

    render() {
        let icon = 'doughnut-chart';
        let title = 'YTD Revenue';
        let footnote = new Date().toUTCString();
        let content = this.renderChart();
        return (
            <Dashlet icon={icon} title={title} footnote={footnote}>
                {content}
            </Dashlet>
        );
    }

    renderChart() {
        let data = this.state.data;
        return (
            <PieChart height={250} data={data}>
                <Tooltip/>
                <PieChart.PieSeries key={0} accessor="Y2019" nameAccessor='product' innerRadius='50%' />
            </PieChart>
        );
    }
    async fetchData() {
        let products = {
            '浪漫華爾滋': 3200,
            '玫瑰訴情': 2500,
            '加州陽光': 2520,
            '盒花酒禮': 4500,
            '浪漫粉聖誕樹': 4000
        };
        let r2019 = random.normal(2000, 500);
        let r2018 = random.normal(2000, 500);
        let r2017= random.normal(2000, 500);

        let data = _.map(products, (price, name) => {
            let datum = {
                product: name,
                'Y2019': price * Math.floor(r2019()),
                'Y2018': price * Math.floor(r2018()),
                'Y2017': price * Math.floor(r2017()),
            };
            return datum;
        });
        return data;
    }
}
```



## Funnel Chart

```jsx
......

import FunnelChart from '../chart/FunnelChart';
import FunnelSeries from "../chart/cartesian/FunnelSeries";

class PurchaseFunnel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    async componentDidMount() {
        let data = await this.fetchData();
        this.setState({data});
    }

    render() {
        let icon = 'filter';
        let title = 'Purchase Funnel';
        let footnote = new Date().toUTCString();
        let content = this.renderChart();
        return (
            <Dashlet icon={icon} title={title} footnote={footnote}>
                {content}
            </Dashlet>
        );
    }

    renderChart() {
        let data = this.state.data;
        return (
            <FunnelChart height={250} data={data}>
                <Tooltip />
                <FunnelSeries accessor="count" nameAccessor="stage">
                    <FunnelSeries.Label formatter={this.labelFormatter} />
                </FunnelSeries>
            </FunnelChart>
        );
    }

    labelFormatter(text) {
        return text.toUpperCase();
    }

    async fetchData() {
        let data = [
            {
                count: 32,
                stage: 'Awareness'
            }, {
                count: 22,
                stage: 'Interest'
            }, {
                count: 18,
                stage: 'Desire'
            }, {
                count: 15,
                stage: 'Action'
            }
        ];
        return data;
    }
}
```

